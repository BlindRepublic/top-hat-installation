# Directories
config_DIR	= conf.d/
post_DIR 	= post-scripts/
initial_DIR	= initial-setup/$(config_DIR)
anaconda_DIR 	= anaconda/$(config_DIR)

# Files
post		= $(wildcard $(post_DIR)/*)
initial		= $(wildcard $(initial_DIR)/*)
anaconda	= $(wildcard $(anaconda_DIR)/*)

# Destinations
config_DEST	= $(DESTDIR)/etc/
share_DEST	= $(DESTDIR)/$(PREFIX)/share
post_DEST	= $(share_DEST)/anaconda/$(post_DIR)
initial_DEST	= $(config_DEST)/$(initial_DIR)
anaconda_DEST	= $(config_DEST)/$(anaconda_DIR)


ifeq ($(PREFIX),)
	PREFIX	:= /usr/local
endif

default: 
	$(error This makefile is just for installation/uninstallation for now!)

install:
	# Post script configuration
	install -d $(post_DEST)
	for p in $(post); do \
		install -Dm644 $$p $(post_DEST); \
	done
	# Initial setup configuration
	install -d $(initial_DEST)
	for i in $(initial); do \
		install -Dm644 $$i $(initial_DEST); \
	done
	# Anaconda configuration
	install -d $(anaconda_DEST)
	for a in $(anaconda); do \
		install -Dm644 $$a $(anaconda_DEST); \
	done

uninstall:
	# Post script configuration
	rm $(post_DEST)/*top-hat*
	# Initial setup configuration
	for i in $(initial); do \
		rm $(config_DEST)/$$i; \
	done
	# Anaconda configuration
	for a in $(anaconda); do \
		rm $(config_DEST)/$$a; \
	done
